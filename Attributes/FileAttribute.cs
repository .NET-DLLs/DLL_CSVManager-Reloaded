﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL_CSVManager.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class FileAttribute : Attribute
    {
        public string FilePath { get; set; } = null;
        public char Separator { get; set; } = ';';
        public bool HasHeader { get; set; }
    }
}
