﻿using System;

namespace DLL_CSVManager.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class FieldAttribute : Attribute
    {
        public string Heading { get; set; } = null;
        public bool Mapped { get; set; } = true;
        public int Order { get; set; }
        public bool IsAutoIncrement { get; set; }
    }
}
