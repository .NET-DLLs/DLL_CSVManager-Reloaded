﻿using System;
using System.Collections.Generic;

namespace DLL_CSVManager.Extensions
{
    static class ListExtensions
    {
        public static void ForEach<T>(this List<T> values, Action<T, int> action)
        {
            for (int i = 0; i < values.Count; i++)
                action(values[i], i);
        }
    }
}
