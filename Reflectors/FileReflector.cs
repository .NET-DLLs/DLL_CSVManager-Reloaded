﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DLL_CSVManager.Attributes;

namespace DLL_CSVManager.Reflectors
{
    class FileReflector<T> : Reflector
    {
        public IEnumerable<FieldReflector> Fields { get; private set; }

        public string FilePath { get; private set; }
        public char Separator { get; private set; }
        public bool HasHeader { get; private set; }

        public FieldReflector IdField => this.Fields.FirstOrDefault(f => f.IsIdField);

        public FileReflector()
        {
            this.Reflect();
        }

        public sealed override void Reflect()
        {
            FileAttribute fileAttribute = typeof(T).GetCustomAttribute<FileAttribute>();

            if (fileAttribute == null)
                throw new Exception("CSVFileAttribute has to be set!");

            this.FilePath = fileAttribute.FilePath ?? typeof(T).Name + ".csv";
            this.Separator = fileAttribute.Separator;
            this.HasHeader = fileAttribute.HasHeader;

            this.Fields = typeof(T).GetProperties()
                                   .Where(p => p.GetCustomAttribute<FieldAttribute>()?.Mapped ?? true)
                                   .Select(p => new FieldReflector(p))
                                   .OrderBy(f => f.Order);

            this.CheckIdFieldCount();
        }

        private void CheckIdFieldCount()
        {
            int idFieldCount =  this.Fields.Count(f => f.IsIdField);

            if (idFieldCount == 0)
                throw new Exception("No id field found. Please make use of the Id-Attribute.");

            if (idFieldCount > 1)
                throw new Exception("Too many id fields.");
        }
    }
}
