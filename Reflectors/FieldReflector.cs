﻿using System;
using System.Reflection;
using DLL_CSVManager.Attributes;

namespace DLL_CSVManager.Reflectors
{
    class FieldReflector : Reflector
    {
        public PropertyInfo Property { get; }

        public bool IsIdField { get; set; }
        public bool IsAutoIncrement { get; set; }
        public string Heading { get; set; }
        public bool Mapped { get; set; }
        public int Order { get; set; }

        public FieldReflector(PropertyInfo property)
        {
            this.Property = property;

            this.Reflect();
        }

        public sealed override void Reflect()
        {
            FieldAttribute fieldAttribute = this.Property.GetCustomAttribute<FieldAttribute>();
            
            this.Heading = fieldAttribute?.Heading ?? this.Property.Name;
            this.Mapped = fieldAttribute?.Mapped ?? true;
            this.Order = fieldAttribute?.Order ?? 0;
            this.IsAutoIncrement = fieldAttribute?.IsAutoIncrement ?? false;

            this.IsIdField = this.Property.GetCustomAttribute<IdAttribute>() != null;

            if (this.IsIdField && !this.Mapped)
                throw new Exception("ID field needs to be mapped!");
        }
    }
}
