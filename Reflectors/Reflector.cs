﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL_CSVManager.Reflectors
{
    abstract class Reflector
    {
        public abstract void Reflect();
    }
}
