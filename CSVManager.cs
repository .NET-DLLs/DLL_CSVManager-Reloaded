﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using DLL_CSVManager.Reflectors;
using DLL_CSVManager.Extensions;

namespace DLL_CSVManager
{
    public class CsvManager<T> : IDisposable where T : new()
    {
        public string FilePath => this.filePath ?? this.fileReflector.FilePath;

        public bool PersistAfterEachAction { get; set; }
        
        private readonly FileReflector<T> fileReflector = new FileReflector<T>();
        private readonly List<string> currentLines = new List<string>();
        private readonly List<T> currentItems = new List<T>();
        private readonly List<Item<T>> changes = new List<Item<T>>();

        private readonly string filePath;

        public CsvManager(string filePath)
        {
            this.filePath = filePath;

            this.CreateFileIfNotExisting();
            this.ReadItemsFromFile();
        }

        public CsvManager() : this(null)
        {
        } 

        /// <summary>
        /// Inserts an element into the CSV-File
        /// </summary>
        /// <param name="item">Item to add</param>
        public void Insert(T item)
        {
            PropertyInfo idProperty = this.fileReflector.IdField.Property;

            if (this.GetById(idProperty.GetValue(item)) != null)
                throw new Exception("Item already existing.");
            
            if (this.fileReflector.IdField.IsAutoIncrement)
            {
                long id = 1;

                if (this.currentLines.Any()
                        || this.changes.Any())
                    id = this.GetAll()
                             .Select(i => Convert.ToInt64(idProperty.GetValue(i)))
                             .Max() + 1;
                
                idProperty.SetValue(item, Convert.ChangeType(id, this.fileReflector.IdField.Property.PropertyType));
            }
            
            this.changes.Add(new Item<T>(item, State.Inserted));
            this.PersistIfNeeded();
        }

        public T GetFirstOrDefault(Predicate<T> searchOperation) => this.GetAll().FirstOrDefault(item => searchOperation(item));

        /// <summary>
        /// Selects items based on the passed search operation
        /// </summary>
        /// <returns></returns>
        public IEnumerable<T> Get(Predicate<T> searchOperation) => this.GetAll().Where(item => searchOperation(item));

        public IEnumerable<T> GetAll()
        {
            for (int index = 0; index < this.currentLines.Count; index++)
            {
                if (this.currentItems.Count <= index)
                    this.currentItems.Add(this.MapLineToItem(this.currentLines[index]));
                
                if (!this.changes.Any(c => c.Value.Equals(this.currentItems[index])
                                                && c.State == State.Deleted))
                    yield return this.currentItems[index];
            }

            foreach (T item in this.changes.Where(c => c.State == State.Inserted)
                                           .Select(c => c.Value))
                yield return item;
        }

        /// <summary>
        /// Read an single item based on the delivered identifier
        /// </summary>
        /// <param name="value">Id value</param>
        /// <returns>Element of type T</returns>
        public T GetById(object value) => this.GetFirstOrDefault(item => this.fileReflector.IdField.Property.GetValue(item).Equals(value));

        /// <summary>
        /// Delete one or more items
        /// </summary>
        /// <param name="predicate">Operation, which is used to find the items to delete</param>
        public void Delete(Predicate<T> predicate)
        {
            IEnumerable<T> itemsToRemove = this.GetAll().Where(i => predicate(i));

            foreach (T item in itemsToRemove)
                this.changes.Add(new Item<T>(item, State.Deleted));
            
            this.PersistIfNeeded();
        }

        public void Delete(T item)
        {
            T itemToDelete = this.GetById(this.fileReflector.IdField.Property.GetValue(item));

            if (itemToDelete == null)
                return;
            
            this.changes.Add(new Item<T>(itemToDelete, State.Deleted));
            this.PersistIfNeeded();
        }

        /// <summary>
        /// Updates an item
        /// </summary>
        /// <param name="item">Item to update</param>
        public void Update(T item)
        {
            T current = this.GetById(this.fileReflector.IdField.Property.GetValue(item));

            if (current == null)
                throw new Exception("Item not existing!");
            
            foreach (PropertyInfo property in this.fileReflector.Fields.Select(f => f.Property))
                property.SetValue(current, property.GetValue(item));

            this.changes.Add(new Item<T>(current, State.Updated));

            this.PersistIfNeeded();
        }

        /// <summary>
        /// Saves an item
        /// 
        /// <para>
        ///     This method automatically decides whether it should
        ///     add the item or update it
        /// </para>
        /// </summary>
        /// <param name="item">Item to save</param>
        public void Save(T item)
        {
            if (this.GetById(this.fileReflector.IdField.Property.GetValue(item)) == null)
                this.Insert(item);
            else
                this.Update(item);
        }
        
        /// <summary>
        /// Delete all items
        /// </summary>
        public void ClearFile()
        {
            this.currentLines.Clear();
            this.currentItems.Clear();

            this.PersistIfNeeded();
        }

        /// <summary>
        /// Creates the file, if it isn't existing already.
        /// </summary>
        public void CreateFileIfNotExisting()
        {
            if (File.Exists(this.FilePath))
                return;

            using (StreamWriter writer = new StreamWriter(this.FilePath))
                if (this.fileReflector.HasHeader)
                    writer.WriteLine(this.GenerateHeader());
        }

        private void ReadItemsFromFile()
        {
            if (!File.Exists(this.FilePath))
                throw new Exception("File not existing!");

            this.currentLines.Clear();
            this.currentItems.Clear();
            this.changes.Clear();
            
            using (StreamReader reader = new StreamReader(
                                            new BufferedStream(
                                                File.Open(this.FilePath, 
                                                            FileMode.Open, 
                                                            FileAccess.Read))))
            {
                if (this.fileReflector.HasHeader)
                    reader.ReadLine();

                string line;

                while ((line = reader.ReadLine()) != null)
                    if (!string.IsNullOrEmpty(line))
                        this.currentLines.Add(line);
            }
        }
        
        public void Persist()
        {
            if (!this.changes.Any())
                return;
            
            StringBuilder textToWrite = new StringBuilder();
            bool onlyInserts = this.changes.Count(c => c.State == State.Inserted) == this.changes.Count;

            if (onlyInserts)
                this.changes.ForEach(c => textToWrite.AppendLine(this.GenerateLine(c.Value)));

            else
            {
                if (this.fileReflector.HasHeader)
                    textToWrite.AppendLine(this.GenerateHeader());

                foreach (T value in this.GetAll())
                    textToWrite.AppendLine(this.GenerateLine(value));
            }
            
            using (StreamWriter writer = new StreamWriter(this.FilePath, onlyInserts))
                writer.Write(textToWrite);

            this.changes.Clear();
        }

        private T MapLineToItem(string line)
        {
            T item = new T();
            string[] values = line.Split(this.fileReflector.Separator);
            
            this.fileReflector.Fields
                              .Select(f => f.Property)
                              .ToList()
                              .ForEach((info, index) => info.SetValue(item, Convert.ChangeType(values[index], info.PropertyType)));

            return item;
        }

        /// <summary>
        /// Generates the header line
        /// </summary>
        private string GenerateHeader() => string.Join(this.fileReflector.Separator.ToString(), this.fileReflector.Fields.Select(f => f.Heading));

        /// <summary>
        /// Generates an line based on the delivered item
        /// </summary>
        private string GenerateLine(T item) => String.Join(this.fileReflector.Separator.ToString(), this.fileReflector.Fields.Select(f => f.Property.GetValue(item)));

        private void PersistIfNeeded()
        {
            if (this.PersistAfterEachAction)
                this.Persist();
        }

        public void Dispose()
        {
            this.Persist();
            this.currentLines.Clear();
            this.currentItems.Clear();
        }
    }
}
