﻿
namespace DLL_CSVManager
{
    internal enum State
    {
        Unchanged,
        Inserted,
        Updated,
        Deleted
    }

    internal class Item<T> where T : new()
    {
        public T Value { get; }
        public State State { get; set; }
        public long FilePosition { get; set; }

        public Item(T value, State state)
        {
            this.Value = value;
            this.State = state;
        }

        public Item(T value) : this(value, State.Unchanged)
        {
        }
    }
}
